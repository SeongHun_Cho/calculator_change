package Calculator;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.math.BigDecimal;
import java.text.DecimalFormat;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;

public class CalculatorMain {

	public static CalculatorMain calculator;
	protected JFrame frame;
	protected JLabel answer;
	protected JLabel answerRecord;
	protected JLabel reportRecord;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					calculator = new InputedNumberController();
					calculator.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * @wbp.parser.entryPoint
	 */
	public CalculatorMain() {
		JLabel answer = new JLabel("");
		answer.setName("asdasdsa");
		answerRecord = new JLabel("");
		reportRecord = new JLabel("");
		answerRecord.setText("0");
		initialize();
	}

	private void initialize() {
		frame = new JFrame();
		frame.setTitle("calculator");
		frame.getContentPane().setBackground(new Color(188, 188, 188));
		frame.setBackground(new Color(0, 0, 0));
		frame.setSize(446, 700);

		JLabel enSharpLabel = new JLabel("EN#");
		enSharpLabel.setBackground(new Color(0, 191, 255));
		enSharpLabel.setForeground(new Color(135, 206, 235));
		enSharpLabel.setBounds(23, 49, 293, 119);
		frame.getContentPane().add(enSharpLabel);

		//계산 결과 나오는 라벨 출력
		answerRecord.setHorizontalAlignment(SwingConstants.RIGHT);
		answerRecord.setBounds(18, 58, 399, 33);
		answerRecord.setBorder(new BevelBorder(BevelBorder.LOWERED));
		frame.getContentPane().add(answerRecord);

		//결과 기록이 기록되는 라벨 출력
		reportRecord.setHorizontalAlignment(SwingConstants.RIGHT);
		reportRecord.setBounds(18, 10, 399, 33);
		reportRecord.setBorder(new BevelBorder(BevelBorder.LOWERED));
		frame.getContentPane().add(reportRecord);
		
		// 숫자 . 버튼 구현
		JButton buttonSpot = new JButton(".");
		buttonSpot.setForeground(new Color(0, 0, 0));
		buttonSpot.setBackground(new Color(135, 206, 235));
		buttonSpot.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				((InputedNumberController) calculator).NumberActionListener(".");
			}
		});
		buttonSpot.setBounds(229, 93, 90, 90);	//위치를 나타낸다. (x좌표,y좌표,가로길이,세로길이)
		frame.getContentPane().add(buttonSpot);
		
		// 숫자 1 버튼 구현
		JButton buttonNumber1 = new JButton("1");
		buttonNumber1.setForeground(new Color(0, 0, 0));
		buttonNumber1.setBackground(new Color(135, 206, 235));
		buttonNumber1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				((InputedNumberController) calculator).NumberActionListener("1");
			}
		});
		buttonNumber1.setBounds(19, 427, 90, 90);	//위치를 나타낸다. (x좌표,y좌표,가로길이,세로길이)
		frame.getContentPane().add(buttonNumber1);

		// 숫자 2 버튼 구현
		JButton buttonNumber2 = new JButton("2");
		buttonNumber2.setBackground(new Color(0, 0, 0));
		buttonNumber2.setBackground(new Color(135, 206, 235));
		buttonNumber2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				((InputedNumberController) calculator).NumberActionListener("2");
			}
		});
		buttonNumber2.setBounds(124, 427, 90, 90);
		frame.getContentPane().add(buttonNumber2);

		// 숫자 3 버튼 구현
		JButton buttonNumber3 = new JButton("3");
		buttonNumber3.setForeground(new Color(0, 0, 0));
		buttonNumber3.setBackground(new Color(135, 206, 235));
		buttonNumber3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				((InputedNumberController) calculator).NumberActionListener("3");
			}
		});
		buttonNumber3.setBounds(229, 427, 90, 90);
		frame.getContentPane().add(buttonNumber3);

		// 숫자 4 버튼 구현
		JButton buttonNumber4 = new JButton("4");
		buttonNumber4.setForeground(new Color(0, 0, 0));
		buttonNumber4.setBackground(new Color(135, 206, 235));
		buttonNumber4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				((InputedNumberController) calculator).NumberActionListener("4");
			}
		});
		buttonNumber4.setBounds(19, 315, 90, 90);
		frame.getContentPane().add(buttonNumber4);

		// 숫자 5 버튼 구현
		JButton buttonNumber5 = new JButton("5");
		buttonNumber5.setBackground(new Color(0, 0, 0));
		buttonNumber5.setBackground(new Color(135, 206, 235));
		buttonNumber5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				((InputedNumberController) calculator).NumberActionListener("5");
			}
		});
		buttonNumber5.setBounds(124, 315, 90, 90);
		frame.getContentPane().add(buttonNumber5);

		// 숫자 6 버튼 구현
		JButton buttonNumber6 = new JButton("6");
		buttonNumber6.setForeground(new Color(0, 0, 0));
		buttonNumber6.setBackground(new Color(135, 206, 235));
		buttonNumber6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				((InputedNumberController) calculator).NumberActionListener("6");
			}
		});
		buttonNumber6.setBounds(229, 315, 90, 90);
		frame.getContentPane().add(buttonNumber6);

		// 숫자 7 버튼 구현
		JButton buttonNumber7 = new JButton("7");
		buttonNumber7.setForeground(new Color(0, 0, 0));
		buttonNumber7.setBackground(new Color(135, 206, 235));
		
		buttonNumber7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				((InputedNumberController) calculator).NumberActionListener("7");
			}
		});
		buttonNumber7.setBounds(19, 204, 90, 90);
		frame.getContentPane().setLayout(null);	//왜 여기에만 이게 들어가있지??
		frame.getContentPane().add(buttonNumber7);

		// 숫자 8 버튼 구현
		JButton buttonNumber8 = new JButton("8");
		buttonNumber8.setForeground(new Color(0, 0, 0));
		buttonNumber8.setBackground(new Color(135, 206, 235));
		buttonNumber8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				((InputedNumberController) calculator).NumberActionListener("8");
			}
		});
		buttonNumber8.setBounds(124, 204, 90, 90);
		frame.getContentPane().add(buttonNumber8);

		// 숫자 9 버튼 구현
		JButton buttonNumber9 = new JButton("9");
		buttonNumber9.setForeground(new Color(0, 0, 0));
		buttonNumber9.setBackground(new Color(135, 206, 235));
		buttonNumber9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				((InputedNumberController) calculator).NumberActionListener("9");
			}
		});
		buttonNumber9.setBounds(229, 204, 90, 90);
		frame.getContentPane().add(buttonNumber9);

		// 숫자 0 버튼 구현
		JButton buttonNumber0 = new JButton("0");
		buttonNumber0.setBackground(new Color(0, 0, 0));
		buttonNumber0.setBackground(new Color(135, 206, 235));
		buttonNumber0.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				((InputedNumberController) calculator).NumberActionListener("0");
			}
		});
		buttonNumber0.setBounds(124, 539, 90, 90);
		frame.getContentPane().add(buttonNumber0);
		
		// 초기화 버튼 구현
		JButton buttonClear = new JButton("C");
		buttonClear.setForeground(new Color(0, 0, 0));
		buttonClear.setBackground(new Color(135, 206, 235));
		buttonClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				((InputedNumberController) calculator).ClearListener();
			}
		});
		buttonClear.setBounds(19, 539, 90, 90);
		frame.getContentPane().add(buttonClear);
		
		// 지우기 버튼 구현
		JButton buttonBackspace = new JButton("←");
		buttonBackspace.setForeground(new Color(0, 0, 0));
		buttonBackspace.setBackground(new Color(135, 206, 235));
		buttonBackspace.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				((InputedNumberController) calculator).DeleteListener();
			}
		});
		buttonBackspace.setBounds(229, 539, 90, 90);
		frame.getContentPane().add(buttonBackspace);
		
		//더하기 버튼 구현
		JButton buttonPlus = new JButton("+");
		buttonPlus.setForeground(new Color(0, 0, 0));
		buttonPlus.setBackground(new Color(135, 206, 235));
		buttonPlus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				((InputedNumberController) calculator).CalActionListener("+");
			}
		});
		buttonPlus.setBounds(334, 204, 90, 90);
		frame.getContentPane().add(buttonPlus);
		
		//빼기 버튼 구현
		JButton buttonMinus = new JButton("-");
		buttonMinus.setForeground(new Color(0, 0, 0));
		buttonMinus.setBackground(new Color(135, 206, 235));
		buttonMinus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				((InputedNumberController) calculator).CalActionListener("-");
			}
		});
		buttonMinus.setBounds(334, 315, 90, 90);
		frame.getContentPane().add(buttonMinus);
		
		//곱셈 버튼 구현
		JButton buttonMultiple = new JButton("*");
		buttonMultiple.setForeground(new Color(0, 0, 0));
		buttonMultiple.setBackground(new Color(135, 206, 235));
		buttonMultiple.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				((InputedNumberController) calculator).CalActionListener("*");
			}
		});
		buttonMultiple.setBounds(334, 427, 90, 90);
		frame.getContentPane().add(buttonMultiple);
		
		//나눗셈 버튼 구현
		JButton buttonDivision = new JButton("÷");
		buttonDivision.setForeground(new Color(0, 0, 0));
		buttonDivision.setBackground(new Color(135, 206, 235));
		buttonDivision.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				((InputedNumberController) calculator).CalActionListener("÷");
			}
		});
		buttonDivision.setBounds(334, 93, 90, 90);
		frame.getContentPane().add(buttonDivision);
		
		//= 버튼 구현
		JButton buttonEqual = new JButton("=");
		buttonEqual.setForeground(new Color(0, 0, 0));
		buttonEqual.setBackground(new Color(135, 206, 235));
		buttonEqual.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				((InputedNumberController) calculator).EqualListner();
			}
		});
		buttonEqual.setBounds(334, 539, 90, 90);
		frame.getContentPane().add(buttonEqual);
		
		// 키를 눌렀을 때의 동작에 대해서
		frame.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent event) {
				char inputedKey = event.getKeyChar();
				int key = event.getKeyCode();

				if (inputedKey >= '0' && inputedKey <= '9') {
					String keyString = "";
					keyString += inputedKey;
					((InputedNumberController) calculator).NumberActionListener(keyString);
				}

				else if (key == KeyEvent.VK_ENTER) {

				}
			}

		});
	}
}

package Calculator;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.List;
import java.util.ArrayList;

public class InputedNumberController
		extends CalculatorMain {

	String number = "0"; // 값을 받는용 문자열
	String numberFront = "0"; // 숫자확정용 문자열(첫번째 숫자는 0으로 초기화하기)
	String numberBack = ""; // 숫자확정용 문자열2
	String operation = ""; // 연산자용 문자열
	String oper = ""; //연산자 확정용 문자열
	String answerLabel = ""; //값 출력용 문자열
	String recordLabel = ""; // 기록 라벨용 문자열
	String value = ""; // 마지막 계산을 여기에 저장할것
	
	

	// 숫자 입력받는 리스너
	public void NumberActionListener(String number) 
	{
		//맨처음에 값을 넣는게 아닐때
		//숫자를 넣기 전에 0인지 확인해서 처음에 0일때 0반복막고 초기화해서 숫자로 덮어쓰기
		if(this.number.equals("0")&&(NumberCompare(number)>=0))
		{
			this.number="";
		}
		
		if(!this.number.equals("")) 
		{
			//숫자의 마지막이 .이고 .이 들어오면 .을 입력받지 않는다.
			if(String.valueOf(this.number.charAt(this.number.length()-1)).equals(".")&&number.equals("."))
			{
				number="";
			}
		}
		
		//여기서 number문자열에 입력받은 값을 넣는다.
		this.number += number;
		answerRecord.setText(this.number);
	}
	
	//연산자 입력받는 리스너
	public void CalActionListener(String cal)
	{
		//-----------------처음에는 일단 숫자를 문자열에 넣어서 확정한다--------------------
		//front와 Back이 다 초기값이고 입력된값이 0이 아니면 front에 값을 넣는다
		if(this.numberBack.equals("")&&this.numberFront.equals("0")&&!number.equals("0"))
		{
			this.numberFront=this.number;
		}
		//그 외의 경우엔 back에 값을 넣는다
		else
		{
			this.numberBack=this.number;
		}
		
		//-----------------------------연산자값을 받기--------------------------------
		//맨처음에는 연산자와 front숫자를 같이 출력한다.
		if(operation.equals(""))
		{
			operation=cal;
			recordLabel+=this.numberFront+cal;
		}
		//맨 마지막이 연산자면 마지막에 그 연산자를 지우고 새로운 걸로 채워넣는다
		else if(StringLast(recordLabel).equals("+")||StringLast(recordLabel).equals("-")
				||StringLast(recordLabel).equals("*")||StringLast(recordLabel).equals("÷"))
		{
			recordLabel=StringLastOut(recordLabel);
			operation=cal;
			recordLabel+=operation;
		}
		//숫자값이 2개가 다 있는 경우에는 그 전 부분 계산하고 덮어씌우기
		else
		{
			String result = ArithmeticMethod(numberFront,numberBack,operation);
			numberFront=result;
			operation=cal;
		}
		//숫자값이 2개가 다 있을 때와 숫자가 하나 없을 때 연산자만 있을 때
		
		reportRecord.setText(recordLabel);
		this.number="0";
	}
	
	//=값 입력받는 리스너
	public void EqualListner()
	{
		
	}
	
	//초기화 메소드
	public void ClearListener()
	{
		
	}
	
	//하나씩 지우는 메소드
	public void DeleteListener()
	{
		
	}
	
	
	//=============================편의용 메소드====================================//
	
	//사칙연산 계산 메소드
	public String ArithmeticMethod(String num1, String num2, String cal)
	{
		String result = "";
		//BigDecimal을 써야 제대로 계산 된다. 
		BigDecimal bigNumberFront = new BigDecimal(num1);
		BigDecimal bigNumberBack = new BigDecimal(num2);
		switch(cal)
		{
		case "+":
			result = bigNumberFront.add(bigNumberBack).toString();
			break;
		case "-":
			result = bigNumberFront.subtract(bigNumberBack).toString();
			break;
		case "÷":
			if(bigNumberBack.toString().equals("0")) 
			{
				return "0으로 나눌 수 없습니다.";
			}
			result = bigNumberFront.divide(bigNumberBack,3,RoundingMode.HALF_EVEN).toString();	
			break;
		case "*":
			result = bigNumberFront.multiply(bigNumberBack).toString();
			break;
		}
		return result;
	}
	
	//숫자인지 확인 메소드(문자가 들어오면 음수 반환)
	public int NumberCompare(String number) 
	{
		int num;
		try
		{
			num=Integer.parseInt(number);
			return num;
		}
		catch(Exception e)
		{
			return -1;
		}
	}
	
	//정수형인지 확인해서 소수점 밑으로 0이 추가되는 거 제한하는 거랑, 소수점 자리수 제한
	public String LocationConfirm(String number) 
	{
		int num1;
		double num2;
		try
		{
			num1 = Integer.parseInt(number);
			return String.valueOf(num1);
		}
		catch(Exception e)
		{
			num2 = Double.valueOf(number);
			return String.valueOf(num2);
		}
	}
	
	//스트링에서 마지막 숫자를비교하는 함수
	public String StringLast(String value)
	{
		String last = value.substring(value.length()-1,value.length());
		return last;
	}
	
	//스트링에서 마지막 문자열을 빼는 함수
	public String StringLastOut(String value)
	{
		String lastOut=value.substring(0,value.length()-1);
		return lastOut;
	}
}